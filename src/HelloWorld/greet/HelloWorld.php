<?php

namespace HelloWorld\HelloWorld\greet;

/**
 * Hi~
 */
class HelloWorld
{

    /**
     * Greetings
     *
     * @return string
     */
    public function greet()
    {
        return "Hello world ! Im beyond sea~";
    }
}